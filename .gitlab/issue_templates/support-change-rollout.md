# GitLab Support: Process Change Rollout Plan
# `Process Change Title`

## The Story
*Write here the words you want the managers to use in explaining the change
 fully - what's changing, how it's changing, why it's changing. This is the best
 opportunity to ensure consistency across regions, managers and individual
 users.*

## The Roles

| Role | Description |
| --- | --- |
| Champions | <!-- For example: managers, staff engineers and support engineers -->*Who specifically are the champions for this change?* |
| Users | <!-- For example: all support engineers-->*Who are the users of the things being changed?* |
| Impacted Non-Users | <!-- For example: Sales, TAMs -->*Who will be secondarily impacted by the changes?* |

## Schedule
* Rollout to begin on `this date`
* Will the rollout be phased, such as by team or region? If so, describe here
  the schedule and order.
* Adoption complete by `this date`

## Training
*What do the users need to learn and how will they learn it? Do managers need to
deliver training? Are there videos or tutorials or handbook pages or other
materials?*

## Success Determination
*Explain here how and what you will be monitoring to determine the success of
the change. These are typical questions you might want to answer here:*
> * What will success look like?
> * How will you track change adoption?
> * Is there a level of adoption that is required?
> * How will you measure success?
> * What are your targets (measured values that equate to success)?

## Action Plan

1. [ ] Announce the change and include [The Story](#the-story) in the SWIR on
   `date`
1. [ ] Post a message in the [`#support_team-chat`](https://gitlab.slack.com/archives/CCBJYEWAW)
   slack channel (or other support channel as appropriate) announcing the change
   and pointing to the SWIR announcment on `date`
1. [ ] Announce the change and tell [The Story](#the-story) in Team meetings by
   `date`
   - [ ] EMEA team meeting
   - [ ] AMER team meeting
   - [ ] APAC team meeting
1. [ ] Other communications channels
   - [ ] Discuss in 1-1s, telling [The Story](#the-story), by `date`
   - [ ] Other communications channels, if required - for example, post to a TAM
     channel if the TAMs will be impacted non-users
1. [ ] Report back on change adoption, concerns, etc. by `date`

## Follow-Up Plan
*How will you follow-up to understand the results of the change, to
make adjustments appropriately, and to rollback if necessary? These
are typical questions you might want to answer here:*

> * How will results be captured? By whom and by when?
> * What is the plan for considering and making quick improvements?
> * What is the plan should the change be deemed unsuccessful?
>   * Is a rollback feasible, and if so how will it happen?

/assign me
/label ~support-change-rollout